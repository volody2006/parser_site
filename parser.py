# -*- coding: utf-8 -*-
from time import sleep

import requests
from bs4 import BeautifulSoup
from random import choice, uniform


def get_html(url, useragent=None, proxy=None):

    r = requests.get(url, headers=useragent, proxies=proxy)
    return r.text

def get_ip(html):
    soup = BeautifulSoup(html, 'lxml')
    ip = soup.find('span', class_ = 'ip').text.strip()
    ua = soup.find('span', class_ = 'ip').find_next_sibling('span').text.strip()
    print(ip, ua)
    print(('-------------------------------------------------------------------------------'))


def run_main():
    url = 'http://sitespy.ru/my-ip'
    useragents = open('useragents.txt').read().split('\n')
    proxies = open('proxylist.txt').read().split('\n')

    for i in range(10):
        sleep (uniform(3,6))
        # proxy = {'http': 'http://'+choice(proxies)}
        # useragent = {'User-Agent': choice(useragents)}
        # try:
        #     html = get_html(url, useragent, proxy)
        # except requests.exceptions.ProxyError:
        #     continue
        # get_ip(html)


if __name__ == '__main__':
    run_main()
