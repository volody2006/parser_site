# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
from django.utils.encoding import python_2_unicode_compatible

from base.models import CommonAbstractModel

UNKNOWN = 0
ENABLED = 1
DISABLED = 2

STATUS_CHOICES = (
    (UNKNOWN, 'Unknown'),
    (ENABLED, 'Хороший'),
    (DISABLED, 'Плохой'),

)

@python_2_unicode_compatible
class Proxy(CommonAbstractModel):
    """
    Лот
    """

    ip = models.GenericIPAddressField()
    port = models.CharField(max_length=5)
    country = models.CharField(max_length=255, default='')
    protocol_raw = models.CharField(max_length=10, default='HTTP')
    status = models.SmallIntegerField(choices = STATUS_CHOICES, default=UNKNOWN)
    type = models.CharField(max_length=255, null=True, blank=True)
    avito_ban = models.BooleanField(default=False, help_text='Заблокировано в avito')
    avito_ban_last_time_check = models.DateTimeField(help_text='Время последней проверки', blank=True, null=True)
    attempts = models.IntegerField(default=0, help_text='Количество неудавшихся попыток')

    class Meta:
        unique_together = (("ip", "port"),)


    def __str__(self):
        return '%s:%s' % (self.ip, self.port)


    def get_dict(self):
        return {self.protocol_raw: '%s://%s:%s' %(self.protocol_raw, self.ip, self.port)}


DESKTOP = 1
MOBIL = 2

UA_CHOICES = (
    (UNKNOWN, 'Неизвестный'),
    (DESKTOP, 'Дестроп'),
    (MOBIL, 'мобильный'),

)
class UserAgent(models.Model):
    user_agent = models.TextField(unique=True)
    type = models.SmallIntegerField(choices = UA_CHOICES, default=UNKNOWN)

    def __str__(self):
        return self.user_agent



