# -*- coding: utf-8 -*-
import asyncio
from proxybroker import Broker

from proxy_parser.models import Proxy, UNKNOWN


async def save(proxies, filename):
    """Save proxies to a file."""
    with open(filename, 'w') as f:
        while True:
            proxy = await proxies.get()
            if proxy is None:
                break
            proto = 'https' if 'HTTPS' in proxy.types else 'http'
            row = '%s://%s:%d\n' % (proto, proxy.host, proxy.port)
            f.write(row)


async def save_db(proxies):
    while True:
        proxy = await proxies.get()
        if proxy is None:
            break
        proto = 'https' if 'HTTPS' in proxy.types else 'http'
        proxy_db, create = Proxy.objects.get_or_create(ip=proxy.host,
                                                       port = proxy.port,
                                                       defaults={'protocol_raw': proto,
                                                                 'status': UNKNOWN})
        # row = '%s://%s:%d\n' % (proto, proxy.host, proxy.port)


def run_parser_proxy():
    proxies = asyncio.Queue()
    broker = Broker(proxies)
    # types = [('HTTP', ('Anonymous', 'High')), 'HTTPS']
    types = ['HTTPS', 'SOCKS4', 'SOCKS5']
    countries = ['RU', 'BY', 'UA', 'KZ']
    tasks = asyncio.gather(broker.find(types=types, limit=500,
                                       # countries=countries
                                       ),
                           save_db(proxies))
    loop = asyncio.get_event_loop()
    loop.run_until_complete(tasks)


