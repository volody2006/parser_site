# -*- coding: utf-8 -*-
from random import uniform, choice
from time import sleep

import logging
import requests
from bs4 import BeautifulSoup
from ipify import get_ip
from ipify.exceptions import ConnectionError, ServiceError
from proxy_parser.models import Proxy, UNKNOWN, DISABLED, ENABLED, UserAgent, DESKTOP, MOBIL

logger = logging.getLogger(__name__)


def start_import_user_agent():
    ua_import(file_name='desktop.list', type=DESKTOP)
    ua_import(file_name='mobil.list', type=MOBIL)


def ua_import(file_name, type=DESKTOP):
    uas = open(file_name).read().splitlines()
    for ua in uas:
        user_agent, create = UserAgent.objects.get_or_create(user_agent=ua,
                                                             defaults={
                                                                 'type': type
                                                             })


def get_my_ip():
    url = 'http://sitespy.ru/my-ip'
    html = get_html(url)
    ip = get_ip(html)
    return ip


def check_ip():
    API_URI = 'https://api.ipify.org'
    myip = get_ip()
    proxys = Proxy.objects.exclude(status=DISABLED, type = 'NOT Anonim', avito_ban=True)
    for p in proxys:
        proxy = {p.protocol_raw: '%s://%s:%s' % (p.protocol_raw, p.ip, p.port)}
        try:
            ip = get_html(API_URI, proxy=proxy)
        except Exception as e:
            logger.info('check ip: proxy %s BAD, Error: %s', p.ip, e)
            p.status = DISABLED
            p.save()
            continue
        if str(myip) != str(ip):
            logger.info('check ip: my ip %s, proxy %s, real %s. Anonim', myip, p.ip, ip)
            # print('Прокси анонимное')
            p.status = ENABLED
            p.type = 'Anonim'
        else:
            logger.info('check ip: my ip %s, proxy %s, real %s. NOT Anonim', myip, p.ip, ip)
            p.status = ENABLED
            p.type = 'NOT Anonim'
        p.save()


def _check_ip():
    url = 'http://sitespy.ru/my-ip'
    # proxys = Proxy.objects.all()
    proxys = Proxy.objects.filter(status=UNKNOWN)
    my_ip = get_my_ip()
    for p in proxys:
        proxy = {p.protocol_raw: '%s://%s:%s' % (p.protocol_raw, p.ip, p.port)}

        try:
            html = get_html(url, proxy=proxy)
            ip = get_ip(html)
            # logger.info('check ip: my ip %s, proxy %s, real %s', my_ip, p.ip, ip)
            # print(my_ip, '---', p.ip, '---', ip)
            if str(my_ip) != str(ip):
                logger.info('check ip: my ip %s, proxy %s, real %s. Anonim', my_ip, p.ip, ip)
                # print('Прокси анонимное')
                p.status = ENABLED
                p.type = 'Anonim'
            else:
                logger.info('check ip: my ip %s, proxy %s, real %s. NOT Anonim', my_ip, p.ip, ip)
                p.status = ENABLED
                p.type = 'NOT Anonim'
            p.save()

        except Exception as e:
            logger.info('check ip: proxy %s BAD, Error: %s', p.ip, e)
            p.status = DISABLED
            p.save()

            # except (requests.exceptions.ProxyError, AttributeError, requests.exceptions.ChunkedEncodingError):

            # except (
            # requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError):
            #     logger.info('check ip: proxy %s BAD', p.ip)
            #     p.status = DISABLED
            #     p.save()
            #
            #     # get_ip(html)


def get_html(url, useragent=None, proxy=None):
    r = requests.get(url, headers=useragent, proxies=proxy, timeout=10)
    return r.text


# def get_ip(html):
#     soup = BeautifulSoup(html, 'lxml')
#     ip = soup.find('span', class_='ip').text.strip()
#     ua = soup.find('span', class_='ip').find_next_sibling('span').text.strip()
#     return ip
