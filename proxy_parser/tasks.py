# -*- coding: utf-8 -*-

import logging
from datetime import timedelta

from celery.schedules import crontab
from celery.task import task, periodic_task

from base.managers import run_tast_block
from base.models import CeleryTaskRun
from parser_site.settings import PARSER_SERVER
from proxy_parser.managers import check_ip
from proxy_parser.models import Proxy
from proxy_parser.parser import run_parser_proxy

logger = logging.getLogger(__name__)


@task()
@periodic_task(run_every=timedelta(minutes=59))
def start_find_proxy():
    if not PARSER_SERVER:
        logger.info('Задача для сервере, выходим')
        return
    task_name = 'proxy_parser.start_find_proxy'
    time_block = 3530
    if run_tast_block(task_name, time_block):
        logger.info('Задача start_find_proxy заблокирована, выходим')
        return
    celery_task_run = CeleryTaskRun.objects.get(task_name=task_name)
    celery_task_run.is_run = True
    celery_task_run.save()

    logger.info('Ищем новые прокси')
    count = Proxy.objects.all().count()
    run_parser_proxy()
    count_new = Proxy.objects.all().count()
    logger.info('Добавлено новых прокси %s шт', count_new-count)
    celery_task_run.is_run = False
    celery_task_run.save()
    check_proxy.delay()



@task()
def check_proxy():
    logger.info('Проверим прокси')
    check_ip()
