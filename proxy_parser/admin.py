from django.contrib import admin

# Register your models here.
from proxy_parser.models import Proxy, UserAgent


class ProxyAdmin(admin.ModelAdmin):
    # raw_id_fields = ['', ]
    list_display = ('ip', 'port', 'country', 'status', 'type', 'protocol_raw' )
    list_filter = ['status', 'country', 'type', 'protocol_raw', 'avito_ban']
    search_fields = ['ip', ]


admin.site.register(Proxy, ProxyAdmin)

class UAAdmin(admin.ModelAdmin):
    # raw_id_fields = ['', ]
    list_display = ('user_agent', 'type' )
    list_filter = ['type', ]
    # search_fields = ['', ]


admin.site.register(UserAgent, UAAdmin)