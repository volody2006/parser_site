# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from rest_framework import routers
from api import views

router = routers.DefaultRouter()
# router.register(r'users', views.UserViewSet)
# router.register(r'groups', views.GroupViewSet)
router.register(r'lots_all', views.LotAllViewSet)
router.register(r'lots_not_phone', views.LotValidNotPhoheSet, 'lots_not_phone_list')
router.register(r'lots_delete', views.LotDeleteSet, 'lots_delete_list')
router.register(r'avitociti', views.AvitoCitiViewSet)
router.register(r'categorys', views.CategoryViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]