# -*- coding: utf-8 -*-
from datetime import timedelta
from api.managers import *
from celery.task import task, periodic_task

from base.managers import run_tast_block, run_word_time
import logging

from base.models import CeleryTaskRun
from parser_site.settings import PARSER_SERVER

logger = logging.getLogger(__name__)


@task()
@periodic_task(run_every=timedelta(minutes=10))
def start_find_phone_api():
    if not run_word_time():
        logger.info('Начался рабочий день, выходим')
        return
    if PARSER_SERVER:
        logger.info('Задача не для сервера, выходим')
        return
    task_name = 'api.start_find_phone_api'
    time_block = 10 * 59
    if run_tast_block(task_name, time_block):
        logger.info('Задача start_find_phone_api заблокирована, выходим')
        return
    celery_task_run = CeleryTaskRun.objects.get(task_name=task_name)
    celery_task_run.is_run = True
    celery_task_run.save()


    a = GetPhoneLot()
    a.get_phone()
    logger.info('Уснули на 10 минут')

    celery_task_run.is_run = False
    celery_task_run.save()

