# -*- coding: utf-8 -*-

from django.contrib.auth.models import User, Group
from rest_framework import viewsets

import arendasmeni.models
from aparser.models import Lot
from api.serializers import *


# class UserViewSet(viewsets.ModelViewSet):
#     """
#     API endpoint that allows users to be viewed or edited.
#     """
#     queryset = User.objects.all().order_by('-date_joined')
#     serializer_class = UserSerializer
#
#
# class GroupViewSet(viewsets.ModelViewSet):
#     """
#     API endpoint that allows groups to be viewed or edited.
#     """
#     queryset = Group.objects.all()
#     serializer_class = GroupSerializer


class LotAllViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Lot.objects.filter(is_full_parse=True, is_valid_lot=True, available=True).exclude(phone='')
    # serializer = LotSerializer(queryset, context={'request': self.request})
    serializer_class = LotSerializer

class LotValidNotPhoheSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Lot.objects.filter(is_valid_lot=True, phone='', available=True)
    serializer_class = LotSerializer1

class LotDeleteSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Lot.objects.filter(available=False)
    serializer_class = LotDeleteSerializer


class AvitoCitiViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = AvitoCiti.objects.all()
    serializer_class = AvitoCitiSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = arendasmeni.models.Category.objects.all()
    serializer_class = CategorySerializer
