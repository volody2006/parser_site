# -*- coding: utf-8 -*-

from django.contrib.auth.models import User, Group
from rest_framework import serializers

from aparser.models import *
from arendasmeni.models import Category

#
# class UserSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = User
#         fields = ('api_url', 'username', 'email', 'groups')
#
#
# class GroupSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Group
#         fields = ('api_url', 'name')


class LotSerializer(serializers.ModelSerializer):
    image_lot = serializers.StringRelatedField(many=True)
    class Meta:
        model = Lot
        fields = ('id', 'api_url', 'title', 'description', 'seller', 'adress', 'price', 'url',
                  'donor', 'donor_id', 'phone', 'category_arenda_id',
                  'image_lot',
                  'available', )



class LotSerializer1(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Lot
        fields = ('id', 'is_full_parse', 'is_valid_lot', 'phone', 'donor', 'donor_id', 'api_url', 'relative_url',
                  'available', )


class LotDeleteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Lot
        fields = ('id', 'donor', 'donor_id', 'api_url', 'available')


class AvitoCitiSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = AvitoCiti
        fields = '__all__'


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name', 'parent', 'parent_id', 'api_url', )



        # fields = ('id', 'title', 'description', 'seller')
