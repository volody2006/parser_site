# -*- coding: utf-8 -*-
import json
import logging
from random import uniform

import requests
from requests.auth import HTTPBasicAuth
from aparser.parser import CeleniumBot, NotEcho

logger = logging.getLogger(__name__)


class GetPhoneLot:
    def __init__(self):
        self.login = 'admin_parser'
        self.parol = 'KJHlskHlksjHGjd1'
        self.api_url = 'http://parser.marst.ru'
        self.auth = HTTPBasicAuth(self.login, self.parol)

    def get_session(self):
        session = requests.session()
        r = session.get(self.api_url, auth=self.auth, timeout=60)
        print(r.text)
        if r.status_code == 200:
            return session

    def get_api_url(self, url):
        full_url = '%s/%s' % (self.api_url, url)
        session = self.get_session()
        r = session.get(url=full_url, auth=self.auth)
        return r.text

    def get_all_valid_lot_not_phone(self):
        url = 'lots_not_phone'
        all_lots = self.get_api_url(url)
        logger.info(all_lots)
        return json.loads(all_lots)

    def get_phone(self):
        i=0
        all_lots = self.get_all_valid_lot_not_phone()
        if all_lots == []:
            logger.info('Нет лотов, завершаем работу')
            return
        bot = CeleniumBot()
        for lot in all_lots:
            i = i + 1
            if i > uniform(10, 30):
                break
            logger.info(lot)
            data = lot
            relative_url = lot['relative_url']
            try:
                phone = bot.run(url='https://www.avito.ru%s' % relative_url)
            except NotEcho:
                logger.info('Снимаем лот с публикации %s', data['id'])
                self.delete_lot(data)
                continue
            except Exception as e:
                phone = ''
                logger.info('Ошибка! %s', e)

            if phone == '':
                logger.info('Пустой номер телефона? Пропускаем')
                self.delete_lot(data)
                continue
            phone = phone.replace("-", "")
            phone = phone.replace(" ", "")
            logger.info('Получили номер телефона %s', phone)
            self.put_api(data, phone)



        logger.info('Принудительно закрываем драйвер')
        bot.close()
        # bot.close()

    def put_api(self, data, phone):
        data['phone'] = str(phone)
        url = 'http://parser.marst.ru/lots_not_phone/%s/' % data['id']
        data['api_url'] = url
        r = requests.put(url=url, data=data, auth=self.auth, timeout=60)
        logger.info('Обновили лот, статус %s', r.status_code)

    def delete_lot(self, data):
        url = 'http://parser.marst.ru/lots_not_phone/%s/' % data['id']
        data['api_url'] = url
        data['available'] = False
        r = requests.put(url=url, data=data, auth=self.auth, timeout=60)
        logger.info('Обновили лот, статус %s', r.status_code)
