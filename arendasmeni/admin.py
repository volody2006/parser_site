# -*- coding: utf-8 -*-
from mptt.admin import MPTTModelAdmin
from django.contrib import admin
from arendasmeni.models import Category, ArendaCiti


class CategoryAdmin(MPTTModelAdmin):
    list_display = ('pk', 'name')
    search_fields = ['name', ]

admin.site.register(Category, CategoryAdmin)

class ArendaCitiAdmin(admin.ModelAdmin):
    list_display = ('pk', 'name_ru', 'name_en', 'avito')
    search_fields = ['name_ru', 'name_en']

admin.site.register(ArendaCiti, ArendaCitiAdmin)
