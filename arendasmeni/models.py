# -*- coding: utf-8 -*-
from django.db import models

from aparser.models import AvitoCiti
from base.models import CommonAbstractModel
from mptt.models import MPTTModel, TreeForeignKey
import logging

logger = logging.getLogger(__name__)

class Category(MPTTModel, CommonAbstractModel):
    name = models.CharField(max_length=255, default='')
    parent = TreeForeignKey('self', related_name='parent_category',
                            null=True,
                            blank=True)


    def __str__(self):
        return self.name



class ArendaCiti(models.Model):
    name_ru = models.CharField(max_length=100, default='', null=True, blank=True)
    name_en = models.CharField(max_length=100, default='', null=True, blank=True)
    avito = models.ForeignKey(AvitoCiti, null=True, blank=True)


    def __str__(self):
        return self.name_ru