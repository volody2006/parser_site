# -*- coding: utf-8 -*-
import csv

import datetime
from django.template.loader import get_template
from slugify import slugify

from aparser.models import Lot
from arendasmeni.models import Category, ArendaCiti


def import_category():
    file_name = 'arendasmeni/test.csv'
    parse_cvs_file(file_name)

def parse_cvs_file(file):
    data=csv.reader(open(file))
    i = 0
    for row in data:
        print(row)
        if i == 0:
            i = i + 1
            continue
        if row[2]:
            parent = row[2]
            parent_obj, create = Category.objects.get_or_create(id=parent)
        else:
            parent_obj = None
        cat, create  = Category.objects.update_or_create(
            id = row[0],
            defaults={'name': row[1],
                      'parent': parent_obj,
                      }
        )



def import_citi():
    file_name = 'arendasmeni/cities.csv'
    data = csv.reader(open(file_name))
    i = 0
    for row in data:
        print(row)
        if i == 0:
            i = i + 1
            continue

        citi_obj, create = ArendaCiti.objects.update_or_create(
            id=row[0],
            defaults={'name_ru': row[1],
                      'name_en': row[2],
                      }
        )

def sexi_avito_arenda():
    from aparser.models import AvitoCiti
    for citi in ArendaCiti.objects.all():
        name_en = slugify(citi.name_ru)
        try:

            citi.avito = AvitoCiti.objects.get(name_en=name_en)
            citi.save()
            print('+ %s' % name_en)
        except AvitoCiti.DoesNotExist:
            print('- %s' % name_en)
            pass




# https://yandex.ru/support/partnermarket/yml/about-yml.xml#requirements

def generate_xml():
    Template = get_template(template_name='primer.xml')
    render = Template.render(context={'categorys': Category.objects.all(),
                                      'date': datetime.datetime.now(),
                                      'citys': ArendaCiti.objects.exclude(avito=None),
                                      'offers':Lot.objects.filter(is_valid_lot=True),
                                      }, request=None)
    filename = 'test_avito.xml'
    with open(filename, 'w') as f:
        f.write(render)
        f.close()
