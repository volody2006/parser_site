import datetime

from django.http import Http404
from django.shortcuts import render
from django.http import StreamingHttpResponse
# Create your views here.
import csv

from django.template import Context
from django.template.loader import get_template

from aparser.models import Lot
from arendasmeni.managers import generate_xml
from arendasmeni.models import Category, ArendaCiti


class Echo(object):
    """Объект, который реализует только метод записи интерфейса файлового типа.
    """

    def write(self, value):
        """Записать значение, возвращая его, вместо того, чтобы хранить в буфере."""
        return value


# def generate_xml():
#     Template = get_template(template_name='primer.xml')
#     render = Template.render(context={'categorys': Category.objects.all(),
#                                       'date': datetime.datetime.now(),
#                                       'citys': ArendaCiti.objects.exclude(avito=None),
#                                       'offers': Lot.objects.filter(is_valid_lot=True),
#                                       }, request=None)
#     filename = 'test_avito.xml'
#     with open(filename, 'w') as f:
#         f.write(render)
#         f.close()


def some_streaming_xml_last_day_view(request):
    t = get_template(template_name='primer.xml')
    # render = t.render(context=, request=None)
    start_day = datetime.date(datetime.datetime.now().year,
                              datetime.datetime.now().month,
                              datetime.datetime.now().day)
    filename = '%s.xml' % start_day
    lots = Lot.objects.filter(is_valid_lot=True,
                              date_created__gte=datetime.date(datetime.datetime.now().year,
                                                              datetime.datetime.now().month,
                                                              datetime.datetime.now().day))
    lots = lots.exclude(phone='')

    c = Context({
        'categorys': Category.objects.all(),
        'date': datetime.datetime.now(),
        'citys': ArendaCiti.objects.exclude(avito=None),
        'offers': lots,
    })
    response = StreamingHttpResponse(t.render(c),
                                     content_type="application/xml")
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    return response


def some_streaming_xml_custom_day_view(request, year, month, day):
    t = get_template(template_name='primer.xml')
    # render = t.render(context=, request=None)
    start_day = datetime.date(int(year), int(month), int(day))
    filename = '%s.xml' % start_day
    offers = Lot.objects.filter(is_valid_lot=True,
                                available=True,
                                date_created__year=int(year),
                                date_created__month=int(month),
                                date_created__day=int(day)
                                )
    if not offers.exists():
        raise Http404()
    c = Context(
        {
            'categorys': Category.objects.all(),
            'date': datetime.datetime.now(),
            'citys': ArendaCiti.objects.exclude(avito=None),
            'offers': offers,
            # 'offers': Lot.objects.filter(is_valid_lot=True),
        }
    )
    response = StreamingHttpResponse(t.render(c),
                                     content_type="application/xml")
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    return response
