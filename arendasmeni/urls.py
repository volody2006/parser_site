# -*- coding: utf-8 -*-


from django.conf.urls import url

from arendasmeni import views

urlpatterns = [
    url(r'^$', views.some_streaming_xml_last_day_view, name='test'),
    url(r'^(?P<year>[0-9]{4})/(?P<month>[0-9]{2})/(?P<day>[0-9]{2})/', views.some_streaming_xml_custom_day_view, name='test'),
    # url(r'^lot/(?P<pk>[0-9]+)/$', LotDetailView.as_view(), name='lot_view'),

    ]


