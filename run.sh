#!/usr/bin/env bash

docker run -d --hostname my-rabbit -p 15673:15672 -p 5673:5672 rabbitmq:3-management
celery -A parser_site worker --loglevel=INFO --concurrency=1 -n worker1 -B --time-limit=2880