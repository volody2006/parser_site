# -*- coding: utf-8 -*-

import requests
from bs4 import BeautifulSoup
import csv

# url = 'https://www.avito.ru/tomsk/predlozheniya_uslug/transport_perevozki/spetstekhnika'
# url = 'https://www.avito.ru/tomsk/predlozheniya_uslug/transport_perevozki/spetstekhnika?p=4'

def get_html(url):
    r= requests.get(url)
    return r.text

def get_total_pages(html):
    soup = BeautifulSoup(html, 'lxml')

    pages = soup.find('div', class_='pagination-pages').find_all('a', class_="pagination-page")[-1].get('href')
    total_pages = pages.split('=')[1]
    return int(total_pages)


def write_csv(data):
    with open('avito.cvs', 'a') as f:
        write =csv.writer(f)
        write.writerow( (data['title'],
                         data['url'],
                         data['price']) )


def get_page_data(html):
    soup = BeautifulSoup(html, 'lxml')
    ads = soup.find('div', class_="catalog-list").find_all('div', class_="item_table")
    for ad in ads:
        # try:

        date_pyblic = ad.find('div', class_='description').find('div', class_='date c-2').text.strip()
        print(date_pyblic)
        if 'Сегодня' in date_pyblic:
            title = ad.find('div', class_='description').find('h3').text.strip()
            url = ad.find('div', class_='description').find('h3').find('a').get('href')
            price = ad.find('div', class_='description').find('div', class_='about').text.strip()
            price = price.replace(" руб.", "")
            price = price.replace(" ", "")
            data = {'title': title,
                    'url': url,
                    'price': price}
            write_csv(data)
        # except:
        #     pass



def run_main():
    base_url = 'https://www.avito.ru/tomsk/predlozheniya_uslug/transport_perevozki/spetstekhnika'
    page_part = 'p='
    html = get_html(base_url)
    get_page_data(html)


if __name__ == '__main__':
    run_main()