# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division

from django.db import models


class CommonAbstractModel(models.Model):
    """
    Абстрактный класс, с двумя предопределенными полями

        1. Поле даты создания
        2. Поле даты модификации

    Оба поля заполняются автоматически и через админку не редактируются.
    """
    # date_created = models.DateTimeField(default=timezone.now,
    #                                     editable=False,
    #                                     db_index=True)
    # date_changed = AutoDateTimeField(editable=False)

    date_created = models.DateTimeField('Created Date/Time', auto_now_add=True)
    date_changed = models.DateTimeField('Last Update Date/Time', auto_now=True)

    class Meta(object):
        abstract = True


class MyError(CommonAbstractModel):
    name_error = models.CharField(max_length=120, blank=True, null=True, help_text='Ошибка')
    text = models.TextField(blank=True, null=True)
    url = models.TextField(blank=True, null=True, help_text='Урл ошибки')
    # HTTP_REFERER
    http_referer = models.TextField(blank=True, null=True, help_text='HTTP_REFERER')
    request_meta = models.TextField(blank=True, null=True, help_text='request.META')
    file_name = models.CharField(max_length=120, blank=True, null=True, help_text='Имя файла')
    level = models.CharField(max_length=20, blank=True, null=True, help_text='Уровень ошибки')

    def __str__(self):
        return self.name_error.encode('utf-8')


class CeleryTaskRun(CommonAbstractModel):
    task_name = models.CharField(max_length=250, unique=True)
    is_run = models.BooleanField(default=False)
    time_block = models.IntegerField(verbose_name='Время блокировки в секундах', default=0)

    def __str__(self):
        return self.task_name
