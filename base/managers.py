# -*- coding: utf-8 -*-
import datetime

from base.models import CeleryTaskRun
from django.utils import timezone

def run_tast_block(task_name, time_block):
    '''

    :param task_name:
    :param time_block:
    :return:
    Возвращаем True, если задача заблокирована
    '''
    try:
        celery_task_run = CeleryTaskRun.objects.get(task_name=task_name)
        if celery_task_run.is_run is False:
            return False
        date_changed = celery_task_run.date_changed
        if date_changed is None:
            return False
        now = timezone.now()
        offset = datetime.timedelta(seconds=time_block)
        time_delta = now - date_changed
        if date_changed + offset < now:
            return False
        return True

    except CeleryTaskRun.DoesNotExist:
        # Задачи нет, значит и не запущена она
        CeleryTaskRun.objects.create(task_name=task_name,
                                     time_block=time_block,
                                     is_run=False)
        return False

def run_word_time():
    '''Возвращаем старт рабочего дня, чтобы не запускались задачи во время моего рабочего дня'''

    start_time = 11-7
    end_time = 17-7
    now = timezone.now()
    if now.hour < start_time:
        return True
    if now.hour > end_time:
        return True
    return False