# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import print_function
from __future__ import absolute_import
from __future__ import division

from base.models import MyError, CeleryTaskRun
from django.contrib import admin


class MyErrorAdmin(admin.ModelAdmin):
    list_display = ('name_error', 'level', 'url', 'http_referer')
    list_filter = ['level', 'file_name']


admin.site.register(MyError, MyErrorAdmin)


class CeleryTaskRunAdmin(admin.ModelAdmin):
    list_display = ('task_name', 'is_run', 'time_block', 'date_changed')
    list_filter = ['is_run', ]


admin.site.register(CeleryTaskRun, CeleryTaskRunAdmin)