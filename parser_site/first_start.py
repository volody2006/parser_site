# -*- coding: utf-8 -*-
from django.utils.translation import ugettext as _

from arendasmeni.managers import import_category, import_citi, sexi_avito_arenda
from proxy_parser import parser, managers
from proxy_parser.managers import start_import_user_agent


def first_start_run():
    # Загрущаем юзер агенты
    start_import_user_agent()
    # Парсим Прокси первый раз и проверям их
    parser.run_parser_proxy()
    managers.check_ip()
    import_category()
    import_citi()
    sexi_avito_arenda()

first_start_run()