# -*- coding: utf-8 -*-
import io
from selenium import webdriver
from time import sleep
from PIL import Image
from pytesseract import image_to_string
import base64
from selenium.webdriver.common.by import By

class Bot:
    def __init__(self):
        # self.driver = webdriver.Firefox()
        # self.driver = webdriver.Chrome('/home/user/program/MyProject/avito_parser/chromedriver')
        self.driver = webdriver.PhantomJS('/home/user/Загрузки/phantomjs-2.1.1-linux-x86_64/bin/phantomjs')
        self.navigate()

    def navigate(self):
        # url = 'https://www.avito.ru/tomsk/predlozheniya_uslug/uborka_snega_chelyustnoy_ekskavator_-_pogruzchik_jcb_874882372'
        url = 'https://www.avito.ru/tomsk/predlozheniya_uslug/uborka_i_vyvoz_snega_arenda_pogruzchika_traktora_783967318'
        self.driver.get(url)
        # button =self.driver.find_element_by_xpath('//button[@class="button item-phone-button js-item-phone-button button-origin button-origin-blue button-origin_full-width button-origin_large-extra item-phone-button_hide-phone item-phone-button_card js-item-phone-button_card"]')
        button =self.driver.find_element_by_class_name('item-phone-number js-item-phone-number').find_element_by_name('button')
        button.click()
        print('sleep')
        sleep(1)
        self.take_screenshot()
        image = self.driver.find_element_by_xpath('//div[@class="item-phone-big-number js-item-phone-big-number"]//*')
        image_data=self.driver.find_element_by_xpath('//div[@class="item-phone-number js-item-phone-number"]//img').get_attribute('src')
        image_data_base64=image_data.split(',')[1]
        im = Image.open(io.BytesIO(base64.b64decode(image_data_base64)))
        im.save('new.png')
        image = Image.open('new.png')
        print(image_to_string(image))
        # image_decode = base64.b64decode(image_data_base64)
        # print(image_data_base64)
        # print(image_data_base64)
        # # image = self.driver.find_element_by_xpath('/x:html/x:body/x:div[9]/x:div[4]/x:div/x:div/x:div[1]/x:img')
        # location = image.location
        # size = image.size
        # self.crop(location, size)


    def take_screenshot(self):
        self.driver.save_screenshot('avito_screenshot.png')

    def crop(self, location, size):
        image = Image.open('avito_screenshot.png')
        x = location['x']
        y = location['y']

        width = size['width']
        height = size['height']
        # image.crop((x, y, x+width, y+height)).save('tel.gif')
        image.crop((685, 525, 954, 559)).save('tel.png')
        self.tel_recon()

    def tel_recon(self):
        image = Image.open('tel.png')
        print(image_to_string(image))


def run():
    b = Bot()

if __name__ == '__main__':
    run()
