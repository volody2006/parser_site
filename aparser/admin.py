from django.contrib import admin

# Register your models here.
from aparser.models import Lot, Image, AvitoCiti


class LotAdmin(admin.ModelAdmin):
    # raw_id_fields = ['', ]
    list_display = ('title', 'category_arenda', 'donor_id', 'price', 'image_url')
    list_filter = ('is_valid_lot',
                   ('category_arenda', admin.RelatedOnlyFieldListFilter),
                   'citi',
                   )
    # search_fields = ['', ]


admin.site.register(Lot, LotAdmin)


class ImageAdmin(admin.ModelAdmin):
    # raw_id_fields = ['', ]
    list_display = ('lot', 'downloaded_from', 'is_main', 'image')
    list_filter = ['is_main', ]
    # search_fields = ['', ]


admin.site.register(Image, ImageAdmin)

class AvitoCitiAdmin(admin.ModelAdmin):
    # raw_id_fields = ['', ]
    list_display = ('name_en', )
    # list_display = ('name_en', 'arendaciti_set__name_ru' )
    # list_filter = ['is_main', ]
    search_fields = ['name_en', ]


admin.site.register(AvitoCiti, AvitoCitiAdmin)
