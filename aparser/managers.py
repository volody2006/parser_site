# -*- coding: utf-8 -*-
from random import choice

import logging
import requests
from proxy_parser.models import Proxy, ENABLED, UserAgent, DESKTOP, UNKNOWN, MOBIL, DISABLED

logger = logging.getLogger(__name__)


def get_random_valid_proxy():
    proxys = Proxy.objects.filter(status=ENABLED,
                                  avito_ban=False,
                                  type='Anonim')
    logger.info('Валидных прокси %s', proxys.count())
    if proxys.exists():
        proxy = choice(proxys)
        logger.info('Выбрали валидное прокси %s', proxy)
        return proxy
    logger.warning('ВАЛИДНЫЕ ПРОКСИ НЕ НАЙДЕНЫ FALSE')
    return False



def get_random_ua(type=None):
    if type not in [UNKNOWN, DESKTOP, MOBIL]:
        type = DESKTOP
    uas = UserAgent.objects.filter(type=type)
    if uas.exists():
        ua = choice(uas)
        logger.info('%s : выбрали юзер агент %s', type, ua)
        return ua
    return False

def check_proxy_avito(proxy_obj, ua_obj):
    url = 'https://avito.ru'
    useragent = {'User-Agent': ua_obj.user_agent}
    proxy = proxy_obj.get_dict()
    r = requests.get(url, headers=useragent, proxies=proxy)
    print(r.status_code)
    if int(r.status_code) == 200:
        proxy_obj.status = ENABLED
        proxy_obj.save()
        return True
    proxy_obj.status = DISABLED
    proxy_obj.save()
    return False
