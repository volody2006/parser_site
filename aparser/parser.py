# -*- coding: utf-8 -*-
import base64
import json
import logging
import os
from random import uniform
from time import sleep
from django.utils import timezone
from PIL import Image as PImage
import io
import requests
from bs4 import BeautifulSoup
import csv

from pytesseract import image_to_string
from selenium import webdriver

from aparser.managers import get_random_valid_proxy, get_random_ua, check_proxy_avito
from aparser.models import Lot, Image, AvitoCiti
from parser_site.settings import MEDIA_ROOT, BASE_DIR, PARSER_SERVER
from proxy_parser.models import ENABLED, DISABLED, MOBIL, DESKTOP
from arendasmeni.models import Category
from urllib.parse import quote, urlparse, parse_qsl, urlencode, urlunparse

logger = logging.getLogger(__name__)


class NotValidProxy(Exception):
    pass

class StopParser(Exception):
    pass

class NotEcho(Exception):
    '''Если отсутствует объявление'''
    pass


driver_path = '%s/parser_site/phantomjs' % BASE_DIR


class CeleniumBot:
    def __init__(self):
        # service_args = [
        #     '--proxy={}'.format(proxy),
        #     '--proxy-type={}'.format(proxy_type),
        # ]
        # self.driver = webdriver.PhantomJS(driver_path)
        self.driver = webdriver.Chrome()
        # service_args=service_args



    def sleep_random(self):
        p = uniform(4, 20)
        print('спим %s сек.' % p)
        sleep(p)



    def run(self, url):

        self.driver.get(url)
        if self.driver.current_url != url:
            logger.info('Авито перенаправил урл, видно объявление снято')
            raise NotEcho
        media_path = os.path.join(MEDIA_ROOT, 'temp')
        print(media_path)
        self.driver.save_screenshot('%s/avito_screenshot_1.png' % media_path)
        self.sleep_random()
        button = self.driver.find_element_by_xpath(
            '//button[@class="button item-phone-button js-item-phone-button button-origin button-origin-blue button-origin_full-width button-origin_large-extra item-phone-button_hide-phone item-phone-button_card js-item-phone-button_card"]')

        button.click()

        self.sleep_random()
        # image = self.driver.find_element_by_xpath('//div[@class="item-phone-big-number js-item-phone-big-number"]//*')
        image_data = self.driver.find_element_by_xpath(
            '//div[@class="item-phone-number js-item-phone-number"]//img').get_attribute('src')
        image_data_base64 = image_data.split(',')[1]
        im = PImage.open(io.BytesIO(base64.b64decode(image_data_base64)))
        im.save('%s/new.png' % media_path)
        image = PImage.open('%s/new.png' % media_path)
        return image_to_string(image)

    def close(self):
        self.driver.close()

class AvitoParser:
    def __init__(self, url='https://www.avito.ru/rossiya/predlozheniya_uslug/transport_perevozki/spetstekhnika'):
        self.proxies = None
        self.proxy = None
        self.user_agent = None
        self.url = url
        self.headers = {}
        self.url_get_param_clear = False
        # self.driver = webdriver.PhantomJS(driver_path)

    def get_proxy(self):
        proxy_obj = get_random_valid_proxy()
        if proxy_obj is False:
            raise NotValidProxy
        self.proxies = proxy_obj.get_dict()
        self.proxy = proxy_obj

    def bad_proxy(self):
        self.proxy.status = DISABLED
        self.proxy.save()

    def proxy_ban_avito(self):
        self.proxy.avito_ban = True
        self.proxy.avito_ban_last_time_check=timezone.now()
        self.proxy.attempts = self.proxy.attempts + 1
        self.proxy.save()

    def get_user_agent(self):
        self.user_agent = get_random_ua(type=DESKTOP).user_agent
        self.headers = {'User-Agent': self.user_agent}

    def get_mobil_user_agent(self):
        return get_random_ua(type=MOBIL).user_agent

    def check_valid_proxy_avito(self):
        check_url = 'https://avito.ru'
        useragent = {'User-Agent': self.user_agent}
        try:
            r = requests.get(check_url, headers=useragent, proxies=self.proxies, timeout=60)
        except requests.exceptions.ConnectionError:
            logger.info('Прокси не валидное')
            self.bad_proxy()
            return False
        if r.url == 'https://www.avito.ru/blocked':
            logger.warning('BAN Avito. Url https://www.avito.ru/blocked')
            self.proxy_ban_avito()
            return False
        if int(r.status_code) == 200:
            logger.info('Прокси валидное')
            self.proxy.status = ENABLED
            self.proxy.save()
            return True
        if int(r.status_code) == 403:
            logger.warning('BAN Avito. CODE 403')
            self.proxy_ban_avito()
            return False
        logger.info('Хер знает как мы тут оказались, выкиним исключение')
        print('code %s, url %s, text %s' %(r.status_code, r.url, r.text))
        raise StopParser
        # self.bad_proxy()
        # logger.info('Прокси не валидное')
        # return False

    def set_valid_proxy(self):
        self.get_proxy()
        # self.get_user_agent()
        while not self.check_valid_proxy_avito():
            self.get_proxy()

    def valid_get_param(self, url):
        url_parts = list(urlparse(url))
        query = dict(parse_qsl(url_parts[4]))
        try:
            return query['bt'] == 1
        except KeyError:
            return False

    def get_html(self, url):
        self.url_get_param_clear = False
        session = requests.session()
        headers = self.headers
        session.headers.update(headers)
        try:
            r = session.get(url, proxies=self.proxies, timeout=60)
            print('урлы1 %s' % r.url)
            print('урлы2 %s' % url)
            if self.valid_get_param(r.url):
                self.url_get_param_clear = True
            if r.url == 'https://www.avito.ru/blocked':
                logger.warning('BAN Avito? %s', r.url)
                self.bad_proxy()
                self.sleep_random()
                self.set_valid_proxy()
                self.get_user_agent()
                return self.get_html(url)
            if self.ban_avito(r.text):
                logger.warning('BAN Avito? %s', 'Да')
                self.sleep_random()
                self.bad_proxy()
                self.set_valid_proxy()
                self.get_user_agent()
                return self.get_html(url)
            return r.text
        except (requests.exceptions.ProxyError, requests.exceptions.ConnectionError):
            # Все таки плохое прокси попалось (((, баним его и ищем новое
            self.bad_proxy()
            self.set_valid_proxy()
            self.get_user_agent()
            return self.get_html(url)
            # return r.text

    def get_ip(self, html):
        soup = BeautifulSoup(html, 'lxml')
        ip = soup.find('span', class_='ip').text.strip()
        ua = soup.find('span', class_='ip').find_next_sibling('span').text.strip()
        print(ip, ua)
        print(('-------------------------------------------------------------------------------'))

    def ban_avito(self, html):
        soup = BeautifulSoup(html, 'lxml')
        try:
            title = soup.find('h2', {'class': 'firewall-title'}).text.strip()
            return True
        except AttributeError:
            return False

    def get_price(self, price):
        # Возвращаем цену как число или нуль
        price = price.replace(" руб.", "")
        price = price.replace(" ", "")
        if price == '':
            price = 0
        try:
            price = int(price)
        except ValueError:
            price = 0
        return price

    def get_page_data_catalog(self, html):
        list_ads = []
        soup = BeautifulSoup(html, 'lxml')
        try:
            ads = soup.find('div', class_="catalog-list").find_all('div', class_="item_table")
        except AttributeError:
            logger.warning('BAN Avito? %s', self.ban_avito(html))
            # print(soup)
            self.set_valid_proxy()
            return list_ads

        for ad in ads:
            # try:

            date_pyblic = ad.find('div', class_='description').find('div', class_='date c-2').text.strip()
            # print(date_pyblic)
            # Собираем только свежие объявы
            if 'Сегодня' in date_pyblic:
                title = ad.find('div', class_='description').find('h3').text.strip()
                url = ad.find('div', class_='description').find('h3').find('a').get('href')
                price = ad.find('div', class_='description').find('div', class_='about').text.strip()
                price = self.get_price(price)
                data = {'title': title,
                        'url': url,
                        'price': price}
                list_ads.append(data)

        return list_ads

    def write_list_db(self, data):
        url_full = data['url']
        url_full_list = url_full.split('/')
        # citi = url_full_list[1]
        category = url_full_list[2]
        donor_id = int(url_full_list[-1].split('_')[-1])
        url = 'https://www.avito.ru%s' % data['url']
        citi, create = AvitoCiti.objects.get_or_create(name_en=url_full_list[1])
        lot, create = Lot.objects.update_or_create(
            url=url,
            defaults={
                'title': data['title'],
                'price': data['price'],
                'citi': citi,
                'category': category,
                'donor_id': donor_id,
                'relative_url': data['url'],
                # 'category_arenda': data['category_arenda']
            })
        logger.info('Создали лот %s' % lot)
        return lot

    def get_page_data_one(self, html, url=''):
        urls_image_list = []
        soup = BeautifulSoup(html, 'lxml')
        try:
            adress = soup.find('span', class_="item-map-address").text.strip()
        except AttributeError:
            adress = ''
        try:
            urls_image_soup = soup.find_all('div', class_="gallery-img-frame js-gallery-img-frame")
            for image_url in urls_image_soup:
                urls_image_list.append(image_url.get('data-url'))
            url_image_primari = soup.find('div', class_="gallery-img-frame js-gallery-img-frame").get('data-url')
        except AttributeError:
            logger.info('ошибка в картике, урл страницы %s' % url)
            url_image_primari = ''
        try:
            description = soup.find('div', class_="item-description-text").text.strip()
        except AttributeError:
            description = ''
        try:
            seller = soup.find('div', class_="seller-info-name").text.strip()
        except AttributeError:
            seller = ''
        data = {
            'adress': adress,
            'description': description,
            'seller': seller,
            'image': url_image_primari,
            'images': urls_image_list
        }
        return data


    def get_phone(self, url, n=0):
        if PARSER_SERVER:
            return ''
        self.sleep_random()
        m_url = 'https://m.avito.ru%s' % url
        headers = {
            'User-Agent': self.get_mobil_user_agent(),
            'Host': 'm.avito.ru',
        }
        session = requests.session()
        self.set_valid_proxy()
        try:
            resp = session.get(m_url, headers=headers, proxies=self.proxies)
            html = resp.text
            soup = BeautifulSoup(html, 'lxml')
            url_phone = soup.find('div', {'class': 'person-actions js-person-actions padding-top'}).find('a').get(
                'href')
            url_phone_full = 'https://m.avito.ru%s?async' % url_phone
            headers['Referer'] = m_url
            resp = session.get(url_phone_full, headers=headers, proxies=self.proxies)
            parsed_string = json.loads(resp.text)
            phone = parsed_string['phone'].replace(' ', '').replace('-', '')
            return phone
        except (AttributeError, json.decoder.JSONDecodeError, requests.exceptions.ConnectionError) as e:
            logger.error('Ошибка %s, пробуем еще раз получить номер телефона, попытка № %s', e, n)
            n = n + 1
            if n > 2:
                logger.info('Слишком много попыток проверить номер, пропускаем')
                return ''
            return self.get_phone(url, n)


    def run(self):
        self.set_valid_proxy()
        cats = Category.objects.filter(parent=None)
        for category in cats:
            lots = []
            search = category.name
            page_url = '%s?bt=1&q=%s' % (self.url, quote(search))
            html = self.get_html(page_url)
            list_ads = self.get_page_data_catalog(html)
            if list_ads == []:
                self.sleep_random()
                continue

            for ad in list_ads:
                ad['category_arenda'] = category
                #   Записали их в базу
                lot_temp = self.write_list_db(ad)
                if lot_temp.is_full_parse:
                    logger.info('Объявление полность отпарсено, пропускаем %s - %s', lot_temp.pk, lot_temp.title)
                    continue
                lots.append(lot_temp)

            for lot in lots:
                self.set_valid_proxy()
                logger.info('Обновляем лот %s - %s', lot.pk, lot.title)
                html = self.get_html(lot.url)
                #   Получили объяву со страницы объявления
                data = self.get_page_data_one(html, lot.url)
                lot.description = data['description']
                lot.seller = data['seller']
                lot.adress = data['adress']
                lot.image_url = data['image']
                lot.is_full_parse = True
                lot.available = True
                if lot.category_arenda is None:
                    lot.category_arenda = category
                lot.save()
                self.load_images(lot, data['images'])
                valid_lot = self.get_valid_lot_not_phone(lot)
                if valid_lot:
                    logger.info('Лот %s валидный, пробуем пролучить номер телефона', lot.pk)
                    phone = self.get_phone(lot.relative_url)
                    lot.phone = phone
                else:
                    logger.info('Лот %s не валидный', lot.pk)
                lot.is_valid_lot = valid_lot

                lot.save()
                self.sleep_random()
            self.sleep_random()

        print('END')

    def sleep_random(self):
        p = uniform(3, 10)
        print('спим %s сек.' % p)
        sleep(p)

    def load_images(self, lot, images):
        is_main = True
        for i in images:
            url = 'http:%s' % i
            # 'http://37.img.avito.st/640x480/3306661937.jpg'
            image = Image.fetch(url, lot, is_main)
            if image is None:
                continue
            image.valid = image.get_valid()
            image.save()
            if is_main:
                if image.get_valid():
                    is_main = False

    def get_valid_lot(self, lot):

        # Цена больше нуля
        if lot.price == 0:
            return False
        # Картинка есть
        if lot.relative_url == '':
            return False
        if lot.seller == '':
            return False
        if lot.phone == '':
            return False
        if lot.get_image() == []:
            return False
        return True

    def get_valid_lot_not_phone(self, lot):

        if lot.price == 0:
            return False
        # Картинка есть
        if lot.relative_url == '':
            return False
        if lot.seller == '':
            return False
        if lot.get_image() == []:
            return False
        return True
