# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import logging
import os
import uuid
import requests
from django.core.files.base import ContentFile
from django.db import models
from django.utils import six
from django.utils.encoding import python_2_unicode_compatible
from base.models import CommonAbstractModel
from PIL import Image as PI
# from arendasmeni.models import Category


logger = logging.getLogger(__name__)

class AvitoCiti(models.Model):
    name_en = models.CharField(max_length=255,
                               verbose_name='Назван', unique=True)
    def __str__(self):
        return self.name_en


@python_2_unicode_compatible
class Lot(CommonAbstractModel):
    """
    Лот
    """
    title = models.CharField(max_length=255,
                            verbose_name='Название товара', blank=True, db_index=True)
    description = models.TextField(verbose_name='Описание',
                                   blank=True, db_index=True)
    seller = models.CharField(max_length=200, verbose_name=u'Продавец', null=True, blank=True)
    adress = models.CharField(max_length=200, verbose_name=u'Адрес', null=True, blank=True)
    category = models.TextField(verbose_name=u'Категория',
                                null=True, blank=True)
    price = models.PositiveIntegerField(null=True, blank=True, verbose_name='цена')
    url = models.TextField(unique=True)
    donor = models.CharField(max_length = 100, help_text='Сайт донор', default='avito')
    donor_id = models.IntegerField(null=True, blank=True)
    citi = models.ForeignKey(AvitoCiti, help_text='Город', null=True, blank=True)
    image_url = models.TextField(default='', null=True, blank=True)
    relative_url = models.TextField(default='', null=True, blank=True)
    phone = models.CharField(max_length = 15, help_text='телефон', default='', null=True, blank=True)
    is_full_parse = models.BooleanField(default=False)
    is_valid_lot = models.BooleanField(default=False)
    category_arenda = models.ForeignKey('arendasmeni.Category', null=True, blank=True)
    available = models.BooleanField(default=True)


    def __str__(self):
        return self.title

    def name(self):
        return self.title

    def get_image(self):
        return Image.objects.filter(lot=self, valid=True)

    def get_url(self):
        return self.url

    def get_citi(self):
        return self.citi.arendaciti_set.get(avito=self.citi)

    def get_images(self):
        return Image.objects.filter(lot=self)



    def image_delete(self):
        for image in self.get_images():
            # Удаление изображений
            logger.debug('Remove image %s %s' % (self.seller, image.pk))
            try:
                image.image.delete()
                image.delete()
                self.image.delete()
            except:
                pass



@python_2_unicode_compatible
class Image(CommonAbstractModel):
    """
    картинка
    """

    # image = models.ImageField(upload_to=upload_to(),
    image = models.ImageField(upload_to='uploads/%Y/%m/%d/',
                              verbose_name=u'Картинка', blank=True, null=True)
    downloaded_from = models.TextField(u'Скачано с', blank=True, null=True)
    is_main = models.BooleanField(default=False)
    lot = models.ForeignKey('Lot', related_name='image_lot', blank=True, null=True)
    is_crop = models.BooleanField(default=False)
    valid = models.BooleanField(default=False, help_text='Изображение валидно?')

    def __str__(self):
        return self.image.url
    #
    # def check_image_format(self):
    #     """
    #     Проверяет, является ли прикреплённый к модели файл корректным
    #     изображением.
    #
    #     Возвращает True в случае, если является. Если не является, генерируется
    #     исключение ValueError.
    #
    #     """
    #     if not bool(self.image):
    #         raise ValueError('No file presented')
    #     if not os.path.exists(self.image.path):
    #         raise ValueError('Presented file is not exists in file system')
    #
    #     get_image_size(self.image)
    #
    #     return True
    def get_valid(self):
        '''
        Узнаем валидно ли изображние.
        Валидность, длина не больше в два раза ширины,
        меньшая сторона не меньше 300
        :return:
        '''
        try:
            image_file=PI.open(fp=self.image.path)
        except ValueError:
            return False
        width = image_file.width
        height = image_file.height
        if width/height > 2:
            return False
        if width < 300:
            return False
        if height < 300:
            return False
        if self.is_crop == False:
            return False
        return True

    def image_crop(self):
        if self.is_crop:
            return
        file_name = self.image.path
        image_file = PI.open(fp=file_name)
        width, height = image_file.size
        image = image_file.crop((0, 0, width, height-50))
        image.save(file_name)
        self.is_crop = True
        self.save()



    def image_resize(self):
        # if self.lot.seller.load_image is False:
        #     return
        file_name = self.image.path
        filter = PI.ANTIALIAS
        image_file = PI.open(fp=file_name)
        # width = image_file.width
        # height = image_file.height
        # if width > MAX_IMAGE_SIZE[0]:
        #     width = MAX_IMAGE_SIZE[0]
        # if height > MAX_IMAGE_SIZE[1]:
        #     height = MAX_IMAGE_SIZE[1]
        image = image_file.resize(image_file.size, filter)
        image.save(file_name)

    def load(self):
        try:
            response = requests.get(self.downloaded_from)
            logger.info('Connected with URL and got response: %d' % response.status_code)

            if response.status_code != 200:
                logger.info('Failed to fetch %s due HTTP error %s' % (self.downloaded_from, response.status_code))
                return None

            # Теперь нужно проверить, является ли файл картинкой
            fp = six.BytesIO(response.content)
            try:
                PI.open(fp).verify()
                del fp
            except Exception:
                logger.info('Pillow thinks that %s is not an image file' % self.downloaded_from)
                return None

            ext = (os.path.splitext(self.downloaded_from)[1].strip('.').lower() or 'jpg').split('?')[0]

            if ext not in ['jpg', 'jpeg', 'gif', 'png']:
                ext = 'jpg'

            if not ext:
                logger.info('[Unable determine file extension - it\'s abnormal]')

            filename = '%s.%s' % (uuid.uuid4(), ext)

            logger.info('Got image filename: %s' % filename)

           # if self.lot.seller.load_image:
            self.image.save(filename, ContentFile(response.content))
            self.save()

        except requests.exceptions.ConnectionError as e:
            logger.info(u'Failed to fetch %s due connection error: %s' % (self.downloaded_from, e))

        # except urllib.ContentTooShortError:
        #     logger.info(u'couldnt retreive full image. path: %s' % self.downloaded_from)
        #
        # except urllib2.HTTPError as e:
        #     logger.info(e.code)
        #
        # except urllib2.URLError as e:
        #     logger.info(e.reason)

        except IOError as e:
            logger.info('IO error: %s' % e)


    @staticmethod
    def fetch(url, lot, is_main=False):

        try:
            image = Image.objects.filter(lot=lot,
                                         downloaded_from=url)[0]
            logger.info(u'This file (%s) had already downloaded' % url)
            if image.is_main == is_main:
                image.is_main = is_main
                image.save()
            image.image_crop()
            return image

        except (Image.DoesNotExist, IndexError):
            logger.info('Файла %s нет, загружаем' % url)

            try:
                response = requests.get(url)
                logger.info('Connected with URL and got response: %d' % response.status_code)

                if response.status_code != 200:
                    logger.info('Failed to fetch %s due HTTP error %s' % (url, response.status_code))
                    return None

                # Теперь нужно проверить, является ли файл картинкой
                fp = six.BytesIO(response.content)
                try:
                    PI.open(fp).verify()
                    del fp
                except Exception:
                    logger.info('Pillow thinks that %s is not an image file' % url)
                    return None

                ext = (os.path.splitext(url)[1].strip('.').lower() or 'jpg').split('?')[0]

                if ext not in ['jpg', 'jpeg', 'gif', 'png']:
                    ext = 'jpg'

                if not ext:
                    logger.info('[Unable determine file extension - it\'s abnormal]')

                filename = '%s.%s' % (uuid.uuid4(), ext)

                logger.info('Got image filename: %s' % filename)

                image = Image(lot=lot,
                              is_main=is_main,
                              downloaded_from=url)
                # if lot.seller.load_image:
                image.image.save(filename, ContentFile(response.content))
                image.save()
                image.image_crop()

                return image


            except requests.exceptions.ConnectionError as e:
                logger.info(u'Failed to fetch %s due connection error: %s' % (url, e))

            # except urllib.ContentTooShortError:
            #     logger.info(u'couldnt retreive full image. path: %s' % url)
            #
            # except urllib2.HTTPError as e:
            #     logger.info(e.code)
            #
            # except urllib2.URLError as e:
            #     logger.info(e.reason)

            except IOError as e:
                logger.info('IO error: %s' % e)
            # except OSError as e:
            #     logger.info('OS error: %s' % e)
        except Exception:
            raise

        return None

    def _createdir(self, dir):
        if not os.path.exists(dir):
            try:
                os.makedirs(dir, 0o755)
            except OSError as e:
                raise EnvironmentError(
                    "Cache directory '%s' does not exist "
                    "and could not be created'" % self._dir)

    def get_image(self):
        if self.image:
            return "%s" % self.image.url
        else:
            return self.downloaded_from

    def __str__(self):
        if self.image:
            return "%s" % self.image
        else:
            return self.downloaded_from

    class Meta:
        unique_together = (("lot", "downloaded_from"),)
