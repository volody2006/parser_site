# -*- coding: utf-8 -*-

import logging
from datetime import timedelta
from celery.task import task, periodic_task
from aparser.parser import AvitoParser
from celery import Celery

from base.managers import run_tast_block
from base.models import CeleryTaskRun
from parser_site.settings import PARSER_SERVER

app = Celery()

logger = logging.getLogger(__name__)


# @task()
@periodic_task(run_every=timedelta(hours=1, minutes=31))
def start_parser_avito():
    if not PARSER_SERVER:
        logger.info('Задача для сервере, выходим')
        return
    task_name = 'aparser.start_parser_avito'
    time_block = 5400
    if run_tast_block(task_name, time_block):
        logger.info('Задача start_parser_avito заблокирована, выходим')
        return
    celery_task_run = CeleryTaskRun.objects.get(task_name=task_name)
    celery_task_run.is_run = True
    celery_task_run.save()


    logger.info('start_parser_avito')
    a = AvitoParser()
    a.run()
    logger.info('end parse avito')
    celery_task_run.is_run = False
    celery_task_run.save()


